# import libraries
import os
import sys
import datetime
import time
import eyed3
import re
import fnmatch
import urllib2
import argparse
import shutil

# import constants from stat library
from stat import * # ST_SIZE ST_MTIME
from xml.sax.saxutils import quoteattr
from xml.sax.saxutils import escape, unescape


xml_escape_table = {
    '"': "&quot;",
    "'": "&apos;",
}
html_escape_table = {
    " ": "%20"
}


parser = argparse.ArgumentParser(description='Process some mp3 files.')
parser.add_argument('--recursive', default='false', help='recursive scan from rootdir')
parser.add_argument('--rootdir', default='/Users/greg/Dropbox/Public/podcasts/', help='base folder to scan for media')
parser.add_argument('--base_site_dir', default='/Users/greg/Dropbox/Public/', help='base folder to scan for media')
parser.add_argument('--rssSiteURL', default='https://dl.dropboxusercontent.com/u/2542792', help='base url for rss file urls')
parser.add_argument('--rssItemURL', default='/podcasts', help='uri /folder for links')
parser.add_argument('--rssImageUrl', default='/logo.png', help='feed logo file')

#parser.add_argument('rssfilename', nargs='?', default='rss.xml', help='rss feed file name')
#parser.add_argument('rssTitle', nargs='?', default='Gregs Dropbox Content', help='rss feed title')
#parser.add_argument('rssDescription', nargs='?', default='Audio Feeds', help='rss feed description')

args = parser.parse_args()
print args


def xml_escape(text):
    return escape(text, xml_escape_table)
def html_escape(text):
    return escape(text, html_escape_table)

# format date method
def formatDate(dt):
    return dt.strftime("%a, %d %b %Y %H:%M:%S +0000")

def xstr(s, fileName):
    if s is None:
        return fileName
    return str(s)

# get the item/@type based on file extension
def getItemType(fileExtension):
    if fileExtension == "aac" or fileExtension == "mp3" or fileExtension == "m4a":
         mediaType = "audio/mpeg"
    elif fileExtension == "mp4" or fileExtension == "avi":
         mediaType = "video/mpeg"
    else:
         mediaType = "audio/mpeg"
    return mediaType


print sys.argv

# constants
# the podcast name
#rssTitle = sys.argv[3]
#rssTitle = args.rssTitle
#print rssTitle

# the podcast description
#rssDescription = "Podcast description"
#rssDescription = sys.argv[4]
#rssDescription = args.rssDescription
#print rssDescription

# the url where the podcast items will be hosted
#rssSiteURL = "https://dl.dropboxusercontent.com/u/2542792"
rssSiteURL = args.rssSiteURL

# the url of the folder where the items will be stored
#rssItemURL = rssSiteURL + "/podcasts/HardcoreHistory"
rssItemURL = rssSiteURL + args.rssItemURL

# the url to the podcast html file
#rssLink = rssSiteURL + "/podcasts.xml"
#rssLink = rssSiteURL + args.rssfilename

# url to the podcast image
#rssImageUrl = rssSiteURL + "/logo.jpg"
rssImageUrl = rssSiteURL + args.rssImageUrl

# the time to live (in minutes)
rssTtl = "60"

# contact details of the web master
rssWebMaster = "greg.a.schultz@gmail.com"

#record datetime started
now = datetime.datetime.now()

# command line options
#    - python createRSFeed.py /path/to/podcast/files /path/to/output/rss
# directory passed in
#rootdir = sys.argv[1]
rootdir = args.rootdir
print rootdir

# output RSS filename
#outputFilename = sys.argv[2]
#outputFilename = args.rssfilename
#print outputFilename

# Main program
#rss master channel file
masterChannelFile = "rss-feeds.xml"
outputMasterFile = open(masterChannelFile, "w")

# open rss file
#outputFile = open(outputFilename, "w")

# write rss header
outputMasterFile.write("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n")
outputMasterFile.write("<rss version=\"2.0\">\n")

#outputFile.write("<channel>\n")
#outputFile.write("<title>" + rssTitle + "</title>\n")
#outputFile.write("<description>" + rssDescription + "</description>\n")
#outputFile.write("<link>" + "/" + rssLink + "</link>\n")
#outputFile.write("<ttl>" + rssTtl + "</ttl>\n")
#outputFile.write("<image>" + rssImageUrl + "</image>\n")
#outputFile.write("<copyright>2015</copyright>\n")
#outputFile.write("<lastBuildDate>" + formatDate(now) + "</lastBuildDate>\n")
#outputFile.write("<pubDate>" + formatDate(now) + "</pubDate>\n")
#outputFile.write("<webMaster>" + rssWebMaster + "</webMaster>\n")



# walk through all files and subfolders 
includes = ['*.mp3','*.mp4','*.m4a']
includes = r'|'.join([fnmatch.translate(x) for x in includes])
excludes = ['.DS_Store']
excludes = r'|'.join([fnmatch.translate(x) for x in excludes])

for path, subFolders, files in os.walk(rootdir, topdown=True):    

    folder = path.split("/")[-1]

    outputFilename = "rss-" + folder + ".xml"

    # open rss file
    outputFile = open(outputFilename, "w")
    rssTitle = "Gregs " + folder + " Aggregation"
    rssDescription = rssTitle
    rssLink = rssSiteURL + "/" + outputFilename


    # write rss header
    outputFile.write("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n")
    outputFile.write("<rss version=\"2.0\">\n")
    outputFile.write("<channel>\n")
    outputFile.write("<title>" + rssTitle + "</title>\n")
    outputFile.write("<description>" + rssDescription + "</description>\n")
    outputFile.write("<link>" + rssLink + "</link>\n")
    outputFile.write("<ttl>" + rssTtl + "</ttl>\n")
    outputFile.write("<image><url>" + rssImageUrl + "</url><title>" + rssTitle + "</title><link>" + rssLink + "</link></image>\n")
    outputFile.write("<copyright>2015</copyright>\n")
    outputFile.write("<lastBuildDate>" + formatDate(now) + "</lastBuildDate>\n")
    outputFile.write("<pubDate>" + formatDate(now) + "</pubDate>\n")
    outputFile.write("<webMaster>" + rssWebMaster + "</webMaster>\n")

    # write channel info to master rss file
    outputMasterFile.write("<channel>\n")
    outputMasterFile.write("<title>" + rssTitle + "</title>\n")
    outputMasterFile.write("<description>" + rssDescription + "</description>\n")
    outputMasterFile.write("<link>" + rssLink + "</link>\n")
    outputMasterFile.write("<ttl>" + rssTtl + "</ttl>\n")
    outputMasterFile.write("<image><url>" + rssImageUrl + "</url><title>" + rssTitle + "</title><link>" + rssLink + "</link></image>\n")
    outputMasterFile.write("<copyright>2015</copyright>\n")
    outputMasterFile.write("<lastBuildDate>" + formatDate(now) + "</lastBuildDate>\n")
    outputMasterFile.write("<pubDate>" + formatDate(now) + "</pubDate>\n")
    outputMasterFile.write("<webMaster>" + rssWebMaster + "</webMaster>\n")


    # exclude/include files
    files = [os.path.join(path, f) for f in files]
    files = [f for f in files if not re.match(excludes, f)]
    files = [f for f in files if re.match(includes, f)]
    files.sort(key=lambda x: os.stat(os.path.join(path, x)).st_ctime)

    for file in files:
        print file

        # split the file based on "." we use the first part as the title and the extension to work out the media type
        fileNameBits = file.split(".")

        fileName = fileNameBits[0].split("/")[-1]
        # get the full path of the file
        fullPath = os.path.join(path, file)
        # get the stats for the file
        fileStat = os.stat(fullPath)
        # find the path relative to the starting folder, e.g. /subFolder/file
        relativePath = fullPath[len(rootdir):]

        #print "fileNameBits: ", fileNameBits
        print "fileName: ", fileName
        print "fullPath: ", fullPath
        #print "fileStat: ", fileStat
        print "relativePath: ", relativePath

        af = eyed3.load(fullPath)
        duration = 0
        try:
            duration = af.info.time_secs
            duration_mins = 0
            duration_secs = 0
        except Exception, e:
            duration = 0
        else:
            duration_mins = duration/60
            duration_secs = duration % 60

        album = ''
        title = ''
        artist = ''
        track_num = ''
        inf = eyed3.core.load(file)
        try:
            album = inf.tag.album
        except Exception, e:
            album = folder
        try:
            title = inf.tag.title
        except Exception, e:
            title = fileName
        try:
            artist = inf.tag.artist
        except Exception, e:
            artist = folder
        try:
            track_num = inf.tag.track_num
        except Exception, e:
            track_num = ''
            
        #date = inf.tag.date
        #print album, title, artist, track_num

        # write rss item
        outputFile.write("<item>\n")
        #outputFile.write("<title>" + fileNameBits[0].replace("_", " ") + "</title>\n")
        outputFile.write("<title>" + xml_escape(xstr(title, fileName)) + "</title>\n")
        outputFile.write("<description>" + xml_escape(xstr(title, fileName)) + "</description>\n")
        outputFile.write("<link>" + xml_escape(rssItemURL + "/" + relativePath) + "</link>\n")
        outputFile.write("<category>" + xml_escape(folder) + "</category>\n")
        outputFile.write("<guid>" + xml_escape(rssItemURL + "/" + relativePath) + "</guid>\n")
        outputFile.write("<pubDate>" + formatDate(datetime.datetime.fromtimestamp(fileStat[ST_MTIME])) + "</pubDate>\n")
        #outputFile.write("<enclosure url=\"" + rssItemURL + "/" + relativePath + "\" length=\"" + str(fileStat[ST_SIZE]) + "\" type=\"" + getItemType(fileNameBits[len(fileNameBits)-1]) + "\" />\n")
        #outputFile.write("<enclosure url=\"" + html_escape(rssItemURL) + "/" + html_escape(relativePath) + "\" length=\"" + str(fileStat[ST_SIZE]) + "\" type=\"" + getItemType(fileNameBits[len(fileNameBits)-1]) + "\" />\n")
        outputFile.write("<enclosure url=\"" + rssItemURL + "/" + relativePath + "\" length=\"" + str(fileStat[ST_SIZE]) + "\" type=\"" + getItemType(fileNameBits[len(fileNameBits)-1]) + "\" />\n")
        outputFile.write("</item>\n")

    outputFile.write("</channel>\n")
    outputFile.write("</rss>")
    outputFile.close()

    outputMasterFile.write("</channel>\n")

    shutil.copy(outputFilename, args.base_site_dir)

# write rss footer
outputMasterFile.write("</rss>")
outputMasterFile.close()
shutil.copy(masterChannelFile, args.base_site_dir)

print "complete"



