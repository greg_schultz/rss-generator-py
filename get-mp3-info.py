# import libraries
import os
import sys
import datetime
import time
import eyed3
import argparse

# import constants from stat library
from stat import * # ST_SIZE ST_MTIME




parser = argparse.ArgumentParser(description='Process some mp3 files.')
parser.add_argument('--recursive', help='recursive scan from rootdir')
parser.add_argument('--rootdir', help='recursive scan from rootdir')
parser.add_argument('filename', help='filename to get info on')
args = parser.parse_args()
print args.recursive


# format date method
def formatDate(dt):
    return dt.strftime("%a, %d %b %Y %H:%M:%S +0000")

# get the item/@type based on file extension
def getItemType(fileExtension):
    if fileExtension == "aac" or fileExtension == "mp3" or fileExtension == "m4a":
         mediaType = "audio/mpeg"
    elif fileExtension == "mp4" or fileExtension == "avi":
         mediaType = "video/mpeg"
    else:
         mediaType = "audio/mpeg"
    return mediaType

#record datetime started
now = datetime.datetime.now()


# command line options
#    - python createRSFeed.py /path/to/podcast/files /path/to/output/rss
# directory passed in
#rootdir = sys.argv[1]
rootdir = args.rootdir

# output RSS filename
#outputFilename = sys.argv[2]

#af = eyed3.load(sys.argv[2])
af = eyed3.load(args.filename)
duration = af.info.time_secs
duration_mins = duration/60
duration_secs = duration % 60
print duration
print duration_mins
print duration_secs


#inf = eyed3.core.load(sys.argv[2])
inf = eyed3.core.load(args.filename)
print inf.tag.album
print inf.tag.title
print inf.tag.artist


# Main program

# open rss file
#outputFile = open(outputFilename, "w")

# walk through all files and subfolders 
#if sys.argv[1] != "false" :
if args.recursive == 'true' :
    for path, subFolders, files in os.walk(rootdir):
        
        for file in files:
     
            # split the file based on "." we use the first part as the title and the extension to work out the media type
            fileNameBits = file.split(".")
            # get the full path of the file
            fullPath = os.path.join(path, file)
            # get the stats for the file
            fileStat = os.stat(fullPath)
            # find the path relative to the starting folder, e.g. /subFolder/file
            relativePath = fullPath[len(rootdir):]

print "--complete"

