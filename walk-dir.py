# !/usr/bin/python

import os
import sys

startingpoint = "."
if sys.argv[1:]:
   startingpoint = sys.argv[1]

for root, dirs, files in os.walk(startingpoint, topdown=False):
    for name in files:
        print(os.path.join(root, name))
    for name in dirs:
        print(os.path.join(root, name))

